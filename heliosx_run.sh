echo "env set bootargs console=ttyS0"
echo "bootz a000000 - 9f00000"

# for GDB interface
# -s -S

./bin/debug/native/arm-softmmu/qemu-system-arm  -machine heliosx -m 256M -nographic -kernel ../heliosx_uboot/u-boot/u-boot.bin -hxfile1 ../heliosx_linux/arch/arm/boot/zImage -hxfile2 ../heliosx_linux/arch/arm/boot/dts/heliosx.dtb 2>&1 | tee debug.log
