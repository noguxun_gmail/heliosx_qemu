# reference
#  http://wiki.qemu.org/Hosts/Linux#Building_QEMU_for_Linux

sudo apt-get install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev

mkdir -p bin/debug/native
cd bin/debug/native
../../../configure --enable-debug --target-list=arm-softmmu
cd ../../..
