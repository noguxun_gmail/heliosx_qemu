/*
 * HeliosX Timer Timer modules.
 *
 * This code is licensed under the GPL.
 */


#include "hw/sysbus.h"
#include "qemu/timer.h"
#include "qemu-common.h"
#include "hw/qdev.h"
#include "hw/ptimer.h"
#include "qemu/main-loop.h"

#define TYPE_HELIOSX_TIMER "heliosx_timer"
#define HeliosxTimer(obj) OBJECT_CHECK(HeliosxTimerState, (obj), TYPE_HELIOSX_TIMER)



typedef struct HeliosxTimerState {
    SysBusDevice parent_obj;

    uint32_t freq;
    ptimer_state *timer;
    MemoryRegion iomem;
    qemu_irq irq;
    uint16_t counter_high;
    uint16_t counter_low;
    uint8_t control;

} HeliosxTimerState;


/* Reset the timer limit after settings have changed.  */
static void hx_timer_recalibrate(HeliosxTimerState *s, int reload)
{
    uint32_t limit;

    limit = s->counter_high;
    limit = (limit << 16 | s->counter_low);

    ptimer_set_limit(s->timer, limit, reload);
}


static void hx_timer_tick(void *opaque)
{
    HeliosxTimerState *s = (HeliosxTimerState *)opaque;

    qemu_irq_raise(s->irq);
}


static uint64_t hx_timer_read(void *opaque, hwaddr offset,
                           unsigned size)
{
    HeliosxTimerState *s = (HeliosxTimerState *)opaque;
    uint16_t val;

    switch(offset) {
    case 0x02:
    	val = s->control;
	break;
    case 0x04:
	val = (uint16_t)ptimer_get_count(s->timer);
	break;
    case 0x06:
	val = (uint16_t)(ptimer_get_count(s->timer) >> 16);
	break;
    default:
        val = 0;
    	printf("xgu reading to no where .... \n");
	break;
    }

    return val;
}

static void hx_timer_write(void *opaque, hwaddr offset,
                        uint64_t value, unsigned size)
{
    HeliosxTimerState *s = (HeliosxTimerState *)opaque;
    uint8_t prescaler;


    switch(offset) {
    case 0x02:
    	// always stop the timer firstly
	ptimer_stop(s->timer);

	s->control = (uint8_t)value;
	// prescaler setting, if 100, then freq is 1M
	prescaler = (uint8_t)(value >> 8);
	if (prescaler == 0) {
        	prescaler = 100;  /* just set a none 0 value */
	}
	s->freq = ((uint32_t )(1000000 * 100))/prescaler;

	hx_timer_recalibrate(s, 1);
	ptimer_set_freq(s->timer, s->freq);
	if (s->control & 0x1) {
	    int mode = ((s->control >> 2) & 0x3);
	    int oneshot;
	    if (mode != 0 || mode != 1 ) {
	    	qemu_log_mask(LOG_GUEST_ERROR, "not supported timer mode");
	    }
	    oneshot = 1 - mode;
	    ptimer_run(s->timer, oneshot);
	}
	break;
    case 0x04:
        s->counter_low = (uint16_t)value;
	break;
    case 0x06:
        s->counter_high = (uint16_t)value;
	break;
    default:
        qemu_log_mask(LOG_GUEST_ERROR,
	              "%s: Bad Offset %x\n", __func__, (int)offset);
    }

    return;
}

static const MemoryRegionOps hx_timer_ops = {
    .read = hx_timer_read,
    .write = hx_timer_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_heliosx_timer = {
    .name = "heliosx_timer",
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT8(control, HeliosxTimerState),
        VMSTATE_UINT16(counter_high, HeliosxTimerState),
        VMSTATE_UINT16(counter_low, HeliosxTimerState),
        VMSTATE_END_OF_LIST()
    }
};

static int hx_timer_init(SysBusDevice *sbd)
{
    DeviceState *dev = DEVICE(sbd);
    HeliosxTimerState *s = HeliosxTimer(dev);
    QEMUBH *bh;

    sysbus_init_irq(sbd, &s->irq);

    bh = qemu_bh_new(hx_timer_tick, s);
    s->timer = ptimer_init(bh);

    memory_region_init_io(&s->iomem, OBJECT(s), &hx_timer_ops, s,
                          "heliosx_timer", 0x100);
    sysbus_init_mmio(sbd, &s->iomem);
    vmstate_register(dev, -1, &vmstate_heliosx_timer, s);
    return 0;
}




static Property hx_timer_properties[] = {
    DEFINE_PROP_UINT32("freq", HeliosxTimerState, freq, 1000000),
    DEFINE_PROP_END_OF_LIST(),
};

static void hx_timer_class_init(ObjectClass *klass, void *data)
{
    SysBusDeviceClass *sdc = SYS_BUS_DEVICE_CLASS(klass);
    DeviceClass *k = DEVICE_CLASS(klass);

    sdc->init = hx_timer_init;
    k->props = hx_timer_properties;
}

static const TypeInfo hx_timer_info = {
    .name          = TYPE_HELIOSX_TIMER,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(HeliosxTimerState),
    .class_init    = hx_timer_class_init,
};

static void arm_timer_register_types(void)
{
    type_register_static(&hx_timer_info);
}

type_init(arm_timer_register_types)
