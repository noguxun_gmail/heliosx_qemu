/*
 * ARM Versatile Platform/Application Baseboard System emulation.
 *
 * Copyright (c) 2005-2007 CodeSourcery.
 * Written by Paul Brook
 *
 * This code is licensed under the GPL.
 */

#include "hw/sysbus.h"
#include "hw/arm/arm.h"
#include "hw/devices.h"
#include "hw/loader.h"
#include "sysemu/sysemu.h"
#include "hw/boards.h"
#include "exec/address-spaces.h"
#include "qemu/error-report.h"


/* Board init.  */
static struct arm_boot_info hx_binfo;

static void hx_load_hxfile(const char *name, uint32_t addr)
{
    int size;
    const char * file = qemu_opt_get(qemu_get_machine_opts(), name);

    if (!file) {
        printf("%s option not there\n", name);
        return;
    }

    size = load_image_targphys(file, addr, hx_binfo.ram_size - (1024*1024));

    if (size > 0) {
       printf("Loaded hx file %s at %x\n", file, addr);
    }
    else {
       printf("Loading failed %s\n", file);
    }
}


static void hx_init(MachineState *machine)
{
    const int board_id = 7777;
    ObjectClass *cpu_oc;
    Object *cpuobj;
    ARMCPU *cpu;
    MemoryRegion *sysmem = get_system_memory();
    MemoryRegion *ram = g_new(MemoryRegion, 1);
    MemoryRegion *spm = g_new(MemoryRegion, 1);
    qemu_irq pic[16];
    DeviceState *dev;
    int n;
    Error *err = NULL;

    if (!machine->cpu_model) {
        machine->cpu_model = "cortex-a8";
    }

    printf("cpu_model: %s \n", machine->cpu_model) ;

    cpu_oc = cpu_class_by_name(TYPE_ARM_CPU, machine->cpu_model);
    if (!cpu_oc) {
        fprintf(stderr, "Unable to find CPU definition\n");
        exit(1);
    }

    cpuobj = object_new(object_class_get_name(cpu_oc));

    if (object_property_find(cpuobj, "has_el3", NULL)) {
        object_property_set_bool(cpuobj, false, "has_el3", &err);
        if (err) {
            error_report_err(err);
            exit(1);
        }
    }

    object_property_set_bool(cpuobj, true, "realized", &err);
    if (err) {
        error_report_err(err);
        exit(1);
    }

    cpu = ARM_CPU(cpuobj);


    memory_region_allocate_system_memory(ram, NULL, "hx.ram", machine->ram_size);
    memory_region_allocate_system_memory(spm, NULL, "hx.spm", (16 * 1024));


    /* SDRAM at address zero.  */
    memory_region_add_subregion(sysmem, 0, ram);

    memory_region_add_subregion(sysmem, 0xEFFF0000, spm);


    /* TODO: try to create a gic, GICD 0xD0091000, GICC 0xD0092000
     * GIC is a little bit complex, not sure how to activate it
     * VIC interrput handler handling
     * According to DSI, the 0xD0081a00 is to select legacy or gic */
    dev = sysbus_create_varargs("heliosx_vic", 0xD0081000,
                                qdev_get_gpio_in(DEVICE(cpu), ARM_CPU_IRQ),
                                qdev_get_gpio_in(DEVICE(cpu), ARM_CPU_FIQ),
                                NULL);
    for (n = 0; n < 16; n++) {
        pic[n] = qdev_get_gpio_in(dev, n);
    }

    /* find out the interrupter */
    sysbus_create_varargs("heliosx_uart", 0xD000A000, pic[2], pic[3], NULL);
    sysbus_create_simple("heliosx_timer", 0xD0081200, pic[8]);

    hx_binfo.ram_size = machine->ram_size;
    hx_binfo.kernel_filename = machine->kernel_filename;
    hx_binfo.kernel_cmdline = machine->kernel_cmdline;
    hx_binfo.initrd_filename = machine->initrd_filename;
    hx_binfo.board_id = board_id;
    hx_binfo.loader_start = (0x08000000 - 0x10000);
    arm_load_kernel(cpu, &hx_binfo);

    hx_load_hxfile("hxfile1", 0x0a000000);
    hx_load_hxfile("hxfile2", 0x09f00000);

    printf("xgu hx_init ending\n");
}


static QEMUMachine hx_machine = {
    .name = "heliosx",
    .desc = "heliosx simulator",
    .init = hx_init,
    .block_default_type = IF_SCSI,
};


static void hx_machine_init(void)
{
    qemu_register_machine(&hx_machine);
}

machine_init(hx_machine_init);


