
#include "hw/sysbus.h"


#define TYPE_HELIOSX_VIC "heliosx_vic"
#define HXVIC(obj) OBJECT_CHECK(HxVicState, (obj), TYPE_HELIOSX_VIC)


typedef struct HxVicState {
    SysBusDevice parent_obj;

    MemoryRegion iomem;
    uint16_t control0;
    uint16_t per_status1;
    uint16_t per_enable1;
    uint16_t per_status2;
    uint16_t per_enable2;
    uint16_t per_status3;
    uint16_t per_enable3;
    uint32_t irq_enable;
    uint32_t fiq_select;
    qemu_irq irq;
    qemu_irq fiq;
} HxVicState;


/* Update interrupts.  */
static void hxvic_update(HxVicState *s)
{
    int set;

    set = (s->per_status1 & s->per_enable1) != 0;
    qemu_set_irq(s->irq, set);

    /* fiq not implemented yet */
}

static void hxvic_set_irq(void *opaque, int irq, int level)
{
    HxVicState *s = (HxVicState *)opaque;

    if (irq >= 16) {
        printf("heliosx not supported interrupt\n");
        return;
    }
// printf ("hxvic_set_irq( irq=%d, level=%d): ", irq, level);

    if (level) {
// printf ("raise irq, new status1 = %d \n", s->per_status1);
        s->per_status1 |= 1u << irq;
    } else {
        s->per_status1 &= ~(1u << irq);
// printf ("clear irq, new status1 = %d \n", s->per_status1);
    }

    hxvic_update(s);
}


static uint64_t hxvic_read(void *opaque, hwaddr offset, unsigned size)
{
    HxVicState *s = (HxVicState *)opaque;

    switch (offset) {
    case 0x10:
        return s->control0;
    case 0x04:
        return s->per_status1;
    case 0x06:
        return s->per_enable1;
    case 0x08:
        return s->per_status2;
    case 0x0a:
        return s->per_enable2;
    case 0xe0:
        return s->per_status3;
    case 0xe2:
        return s->per_enable3;
    default:
        qemu_log_mask(LOG_GUEST_ERROR,
                      "hxvic_read: Bad offset %x\n", (int)offset);
        return 0;
    }
}

static void hxvic_write(void *opaque, hwaddr offset,
                        uint64_t val, unsigned size)
{
    HxVicState *s = (HxVicState *)opaque;

    switch (offset) {
    case 0x10: /* Interrupt Control 0 */
        s->control0 = (uint16_t)val;
	break;
    case 0x04:
        s->per_status1 = s->per_status1 & ~(val);
	break;
    case 0x06:
        s->per_enable1 = (uint16_t)(val) ;
	break;
    case 0x08:
        s->per_status2 = s->per_status2 & ~(val);
	break;
    case 0x0a:
        s->per_enable2 = (uint16_t)val;
	break;
    case 0xe0:
        s->per_status3 = s->per_status3 & ~(val);
	break;
    case 0xe2:
        s->per_enable3 = (uint16_t)val;
	break;
    default:
        qemu_log_mask(LOG_GUEST_ERROR,
                     "hxvic_write: Bad offset %x\n", (int)offset);
        return;
    }
    hxvic_update(s);
}

static const MemoryRegionOps hxvic_ops = {
    .read = hxvic_read,
    .write = hxvic_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static void hxvic_reset(DeviceState *d)
{
    HxVicState *s = HXVIC(d);

    s->per_status1 = 0;
    s->per_status2 = 0;
    s->per_status3 = 0;
    s->per_enable1 = 0;
    s->per_enable2 = 0;
    s->per_enable3 = 0;
    s->control0 = 0;

    hxvic_update(s);
}

static int hxvic_init(SysBusDevice *sbd)
{
    DeviceState *dev = DEVICE(sbd);
    HxVicState *s = HXVIC(dev);

    memory_region_init_io(&s->iomem, OBJECT(s), &hxvic_ops, s, "hxvic", 0x100);
    sysbus_init_mmio(sbd, &s->iomem);
    qdev_init_gpio_in(dev, hxvic_set_irq, 16);
    sysbus_init_irq(sbd, &s->irq);
    sysbus_init_irq(sbd, &s->fiq);
    return 0;
}

static const VMStateDescription vmstate_hxvic = {
    .name = "heliosx_vic",
    .version_id = 1,
    .minimum_version_id = 1,
};

static void hxvic_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    SysBusDeviceClass *k = SYS_BUS_DEVICE_CLASS(klass);

    k->init = hxvic_init;
    dc->reset = hxvic_reset;
    dc->vmsd = &vmstate_hxvic;
}

static const TypeInfo hxvic_info = {
    .name          = TYPE_HELIOSX_VIC,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(HxVicState),
    .class_init    = hxvic_class_init,
};

static void hxvic_register_types(void)
{
    type_register_static(&hxvic_info);
}

type_init(hxvic_register_types)
