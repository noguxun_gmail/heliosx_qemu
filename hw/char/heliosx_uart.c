/*
 * Arm PrimeCell HX_UART UART
 *
 * Copyright (c) 2006 CodeSourcery.
 * Written by Paul Brook
 *
 * This code is licensed under the GPL.
 */

#include "hw/sysbus.h"
#include "sysemu/char.h"

#define TYPE_HELIOSX_UART "heliosx_uart"
#define HX_UART(obj) OBJECT_CHECK(HxUartState, (obj), TYPE_HELIOSX_UART)


#define DO_PRAGMA(x) _Pragma (#x)
#define TODO(x) DO_PRAGMA(message ("TODO - " #x))

TODO("Manage & flip RX/TX IRQ bits in XCPU Periphreal Interrupt Status 1")
// RD0081006h XCPU Peripheral Interrupt Enable 1 (page 19-11)
#define HX_PER_INT_ENABLE1  (0x06)
#define HX_UART_INT_BIT_RX  (1 << 2)
#define HX_UART_INT_BIT_TX  (1 << 3)


/* TODO: XCPU Periphreal Interrupt Status 1 ??? */
// #define HX_UART_INT_RX     (1 << 2)
// #define HX_UART_INT_TX     (1 << 3)

#define HX_UART_INT_TX     (1 << 6)
#define HX_UART_INT_RX     (1 << 4)

/* UART status bits in register D000A62C */
#define HX_UART_TX_EMPTY            (1 << 6)
#define HX_UART_RX_RDY              (1 << 4)
/* #define HX_UART_TX_RDY_NOT_USED     (1 << 5) */  /* Not used */


typedef struct HxUartState {
    SysBusDevice parent_obj;

    MemoryRegion iomem;
    uint16_t stat;
    uint16_t ctrl;
    uint16_t int_level; // 16 bit... XCPU Periphreal Interrupt Status 1 [???]
    uint32_t read_fifo[16]; // FIXME: input/RX FIFO , no output/TX FIFO ??
    int read_pos;
    int read_count;
    int read_trigger;
    CharDriverState *chr;
    qemu_irq irq_rx;
    qemu_irq irq_tx;
} HxUartState;



static void hx_uart_rx_interrupt(HxUartState *s)
{
    uint32_t flags;

    flags = s->int_level & ( s->ctrl & HX_UART_RX_RDY ); // UART status:  a new char is IN
    qemu_set_irq(s->irq_rx, flags != 0);
}


static void hx_uart_tx_interrupt(HxUartState *s)
{
    uint32_t flags;

    flags = s->int_level & ( s->ctrl & HX_UART_TX_EMPTY ); // UART status: the tx buffer is empty
    qemu_set_irq(s->irq_tx, flags != 0);
}


static uint64_t hx_uart_read(void *opaque, hwaddr offset,
                           unsigned size)
{
    HxUartState *s = (HxUartState *)opaque;
    uint32_t c;

    switch (offset) {
    case 0x06E:   /* ARB of the XCPU */
    	return 0x8000;
    case 0x62C:   /* STAT */
        return s->stat;
    case 0x620:   /* RBR */
        c = s->read_fifo[s->read_pos];
        if (s->read_count > 0) {
            s->read_count--;
            if (++s->read_pos == 16)
                s->read_pos = 0;
        }
        if (s->read_count == 0) {
            s->stat &= ~HX_UART_RX_RDY; // clear UART status RX ready 
        }
        if (s->read_count == s->read_trigger - 1) // disable RX irq threshold (now == 1)
            s->int_level &= ~ HX_UART_INT_RX;  // clear XCPU interrupt status 1 UART RX
        hx_uart_rx_interrupt(s);
        if (s->chr) {
            qemu_chr_accept_input(s->chr);
        }
        return c;
    case 0x628:   /* UART Control */
    	return s->ctrl;

    default:
        printf("Bang! not supported register read, offset %x!\n", (uint32_t)offset);
    }

    return 0;
}

static void hx_uart_write(void *opaque, hwaddr offset,
                        uint64_t value, unsigned size)
{
    HxUartState *s = (HxUartState *)opaque;
    unsigned char ch;

    switch (offset) {
    case 0x06E:   /* ARB of the XCPU */
        break;
    case 0x624:   /* TBR */
        ch = value;
        if (s->chr) {
            qemu_chr_fe_write(s->chr, &ch, 1);
	}
        s->int_level |= HX_UART_INT_TX; // trigger interrupt XCPU interrupt status 1 UART TX
	hx_uart_tx_interrupt(s);
	break;
    case 0x628:   /* UART Control */
    	s->ctrl = (uint16_t) value;
	break;
    default:
        printf("Bang! not supported register write, offset %x, value %" PRId64 " size %u !\n", (uint32_t)offset , ( uint64_t) value, size );
    }
}

static int hx_uart_can_receive(void *opaque)
{
    HxUartState *s = (HxUartState *)opaque;

    return s->read_count < 1;
}

static void hx_uart_put_fifo(void *opaque, uint32_t value)
{
    HxUartState *s = (HxUartState *)opaque;
    int slot;

    slot = s->read_pos + s->read_count;
    if (slot >= 16)
        slot -= 16;
    s->read_fifo[slot] = value;
    s->read_count++;
    s->stat |= HX_UART_RX_RDY;

    if (s->read_count == s->read_trigger) {
        s->int_level |= HX_UART_INT_RX; // trigger interrupt XCPU interrupt status 1 UART RX
        hx_uart_rx_interrupt(s);
    }
}

static void hx_uart_receive(void *opaque, const uint8_t *buf, int size)
{
    hx_uart_put_fifo(opaque, *buf); // FIXME: WHAT ABOUT SIZE ?!
}

static void hx_uart_event(void *opaque, int event)
{
    if (event == CHR_EVENT_BREAK)
        hx_uart_put_fifo(opaque, 0x400);
}

static const MemoryRegionOps hx_uart_ops = {
    .read = hx_uart_read,
    .write = hx_uart_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_pl011 = {
    .name = "heliosx_uart",
    .version_id = 2,
    .minimum_version_id = 2,
    .fields = (VMStateField[]) {
        VMSTATE_UINT16(stat, HxUartState),
        VMSTATE_UINT16(ctrl, HxUartState),
        VMSTATE_UINT16(int_level, HxUartState),
        VMSTATE_UINT32_ARRAY(read_fifo, HxUartState, 16),
        VMSTATE_INT32(read_pos, HxUartState),
        VMSTATE_INT32(read_count, HxUartState),
        VMSTATE_INT32(read_trigger, HxUartState),
        VMSTATE_END_OF_LIST()
    }
};

static void hx_uart_init(Object *obj)
{
    SysBusDevice *sbd = SYS_BUS_DEVICE(obj);
    HxUartState *s = HX_UART(obj);

    memory_region_init_io(&s->iomem, OBJECT(s), &hx_uart_ops, s, "heliosx_uart", 0x1000);
    sysbus_init_mmio(sbd, &s->iomem);

    sysbus_init_irq(sbd, &s->irq_rx);
    sysbus_init_irq(sbd, &s->irq_tx);
}

static void hx_uart_realize(DeviceState *dev, Error **errp)
{
    HxUartState *s = HX_UART(dev);

    s->stat |= HX_UART_TX_EMPTY;
    s->stat &= ~HX_UART_RX_RDY;
    s->ctrl = 0;
    s->read_trigger = 1; // trigger RX irq after 1st char in

    /* FIXME use a qdev chardev prop instead of qemu_char_get_next_serial() */
    s->chr = qemu_char_get_next_serial();

    if (s->chr) {
        qemu_chr_add_handlers(s->chr, hx_uart_can_receive, hx_uart_receive,
                              hx_uart_event, s);
    }
}

static void hx_uart_class_init(ObjectClass *oc, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(oc);

    dc->realize = hx_uart_realize;
    dc->vmsd = &vmstate_pl011;
    /* Reason: realize() method uses qemu_char_get_next_serial() */
    dc->cannot_instantiate_with_device_add_yet = true;

}

static const TypeInfo heliosx_uart_arm_info = {
    .name          = TYPE_HELIOSX_UART,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(HxUartState),
    .instance_init = hx_uart_init,
    .class_init    = hx_uart_class_init,
};


static void hx_uart_register_types(void)
{
    type_register_static(&heliosx_uart_arm_info);
}

type_init(hx_uart_register_types)
