#include <stdint.h>



#define TEST_HELIOSX


#ifdef TEST_HELIOSX

#define REG_BASE (0xD000A620)

static volatile uint16_t *rUART_BASE = (uint16_t *)(REG_BASE);
static volatile uint16_t *rUART_RBR = (uint16_t *) (REG_BASE + 0x00);
static volatile uint16_t *rUART_STAT = (uint16_t *)(REG_BASE + 0x0C);
static volatile uint16_t *rUART_TBR = (uint16_t *)(REG_BASE + 0x04);
static volatile uint16_t *rXCPU_ARB =   (uint16_t *)( 0xD000A06E);


void send_char(char c)
{
    int done = 0;
    if ( c == 0xFF ) {
        return;
    }

    while ( done == 0 ) {
        if ((*rUART_STAT) & (1 << 6)) {
            *rUART_TBR = c;
            done = 1;
        }
    }
}

void print_uart0(const char *s) {
    while(*s != '\0') { /* Loop until end of string */
        send_char(*s);
        s++; /* Next char */
    }
}

void c_entry() {
    print_uart0("Hello virtual heliosx world!\r\n");
    while(1) {
        if ((*rUART_STAT) & (1 << 4)) {
            char c = (char)(*rUART_RBR);
            send_char(c);
            if (c == '\r') {
      	        send_char('\n');
            }
        }
    }
}



#else // Testing the PL011

volatile uint32_t * const UART0DR = (uint32_t *)(0x101f1000);
volatile uint32_t * const UART0FR = (uint32_t *)(0x101f1000 + 0x18);

enum {
    RXFE = 0x10,
    TXFF = 0x20,
    TXFE = 0x80
};

void send_char(char c)
{
    while(*UART0FR & TXFF);
    *UART0DR = c;
}

void print_uart0(const char *s) {
    while(*s != '\0') { /* Loop until end of string */
        send_char(*s);
        s++; /* Next char */
    }
}

void c_entry() {
    if (*UART0FR & TXFE) {
      print_uart0("TXFE 1\r\n");
    }
    else {
      print_uart0("TXFE 0\r\n");
    }

    if (*UART0FR & RXFE) {
      print_uart0("RXFE 1\r\n");
    }
    else {
      print_uart0("RXFE 0\r\n");
    }
    print_uart0("Hello world!\r\n");

    while(1) {
        if ((*UART0FR & RXFE) == 0) {
            char c = (char)(*UART0DR);
            send_char(c);
            if (c == '\r') {
      	        send_char('\n');
            }
        }
    }
}

#endif
